//
//  ViewController.swift
//  iRide
//
//  Created by Ansari,Nadeem on 3/15/16.
//  Copyright © 2016 Generation Of Miracles. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{

    @IBOutlet weak var currentLocation: MKMapView!
    
    @IBOutlet weak var destinationLocation: MKMapView!
    
    var location = CLLocationManager()
    
    @IBAction func segmentedControlAction(sender: UISegmentedControl!) {
        
        switch (sender.selectedSegmentIndex) {
        case 0:
            currentLocation.mapType = .Standard
        case 1:
            currentLocation.mapType = .Satellite
        default: // or case 2
            currentLocation.mapType = .Hybrid
    }
    }
    
    @IBAction func startRide(sender: UIButton) {
        if sender.titleLabel?.text == "Start Ride"{
            
            sender.backgroundColor  = UIColor.redColor()
            sender.setTitle("End Ride", forState: .Normal)
        }
        else{
            sender.backgroundColor  = UIColor.greenColor()
            sender.setTitle("Start Ride", forState: .Normal)
        
    }
    }
    @IBOutlet weak var slideOutMenuButton: UIBarButtonItem!

   

    
    override func viewDidLoad() {
        
        if self.revealViewController() != nil {
            slideOutMenuButton.target = self.revealViewController()
            slideOutMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        
        super.viewDidLoad()
        
        location.delegate = self
        location.desiredAccuracy = kCLLocationAccuracyBest
        location.requestWhenInUseAuthorization()
        location.startUpdatingLocation()
        
        currentLocation.showsUserLocation = true
        
        let destination = CLLocationCoordinate2DMake(40.346102, -94.872471)
        // Drop a pin
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = destination
        dropPin.title = "Hy-vee"
        destinationLocation.addAnnotation(dropPin)
        
        
        
        
    }
    
    func locationManager(manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        print(locations)
        let location = locations.last! as CLLocation
        
        
        let startPoint = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let startRegion = MKCoordinateRegion(center: startPoint, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        
        
        self.currentLocation.setRegion(startRegion, animated: true)
        
        
        //self.destinationLocation.setRegion(endRegion, animated: true)
        //self.currentLocation.showAnnotations(self.currentLocation.annotations, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

    
    
    

}

